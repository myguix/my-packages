(define-module (home-configuration isync)
               #:use-module (gnu services)
               #:use-module (gnu home services)
               #:use-module (gnu services configuration)
               #:use-module (guix gexp)
               #:export (isync-account
                          home-isync-service-type))

(define-maybe/no-serialization integer)
(define-configuration/no-serialization
  isync-account
  (name
    (string)
    "Account name.")
  (host
    (string)
    "Hostname")
  (port
    (integer 993)
    "Port")
  (user
    (string)
    "User")
  (passcmd
    (string)
    ""))

(define (serialize-isync-account acc)
  (let ((name (isync-account-name acc))
        (host (isync-account-host acc))
        (port (number->string (isync-account-port acc)))
        (pass (isync-account-passcmd acc))
        (user (isync-account-user acc))
        (make-line (lambda (l) (string-append (car l) " " (car (cdr l)) "\n"))))
    (string-concatenate
      (map make-line
           (list
             `("# Account:" ,name)
             `("IMAPAccount" ,name)
             `("Host" ,host)
             `("Port" ,port)
             `("User" ,user)
             `("PassCmd" ,(string-append "\""  pass "\""))
             '("SSLType" "IMAPS")
             '("CertificateFile" "/etc/ssl/certs/ca-certificates.crt\n")
             `("IMAPStore" ,(string-append name "-remote"))
             `("Account" ,(string-append name "\n"))
             `("MaildirStore" ,(string-append name "-local"))
             '("SubFolders" "Verbatim")
             `("Path" ,(string-append "~/.mail/" name "/"))
             `("Inbox" ,(string-append "~/.mail/" name "/INBOX\n"))
             `("Channel" ,name)
             `("Far" ,(string-append ":" name "-remote:"))
             `("Near" ,(string-append ":" name "-local:"))
             '("Patterns" "*")
             '("Create" "Both")
             '("Expunge" "Both")
             '("SyncState" "*\n\n"))))))

(define (serialize-isync-accounts accs)
  (string-concatenate (map serialize-isync-account accs)))

(define (isync-accounts->file configurations)
  (plain-file "mbsyncrc"
              (serialize-isync-accounts configurations)))

(define (home-isync-service-type isync-accounts)
  (simple-service
   'home-isync
   home-files-service-type
   `((".mbsyncrc" ,(isync-accounts->file isync-accounts)))))

(define-module (my-vm services nginx)
  #:use-module (gnu services web)
  #:export (nginx-301 nginx-proxy))

(define (nginx-301 name)
  (nginx-server-configuration
    (listen '("80" "[::]:80"))
    (server-name `(,name))
    (raw-content
      `(,(string-append "if ($host = " name ") {")
        "    return 301 https://$host$request_uri;"
        "}"
        "return 404;"))))
(define (nginx-proxy proto cert key name addr port)
  (nginx-server-configuration
    (listen '("443 ssl http2"
              "[::]:443 ssl http2"))
    (ssl-certificate cert)
    (ssl-certificate-key key)
    (server-name `(,name))
      (root "/")
      (locations
        (list
          (nginx-location-configuration
            (uri "/")
            (body
              `(,(string-append
                  "proxy_pass " proto "://" addr ":" (number->string port) ";")
                "proxy_set_header        Host $host;"
                "proxy_set_header        X-Real-IP $remote_addr;"
                "proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;"
                "proxy_set_header        X-Forwarded-Proto $scheme;")))))))

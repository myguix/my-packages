(define-module (my-vm system firewall)
  #:use-module (guix i18n)
  #:use-module (guix diagnostics)
  #:use-module (guix records)
  #:use-module (guix gexp)
  #:use-module (gnu services)
  #:use-module (gnu services configuration)
  #:use-module (guix modules)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-1)
  #:export (my-nftables-make-file))





(define (serialize-string field value) value)





(define-configuration
  my-nftables-rule
  (rule
   (string)
   "TODO: structure rules."))

(define (serialize-my-nftables-rule rule)
  (my-nftables-rule-rule rule))





(define (list-of-my-nftables-rules? lst)
  (every my-nftables-rule? lst))

(define (serialize-list-of-my-nftables-rules rules)
  (string-append
   (string-join
    (map
     (lambda (s)
       (string-append "      " (serialize-my-nftables-rule s)))
     rules)
    "\n")
   "\n"))





(define-configuration
  my-nftables-chain
  (name
   (string)
   "Name of the chain.")
  (type
   (string)
   "Type of the chain.")
  (hook
   (string)
   "Hook of the chain.")
  (priority
   (string)
   "Priority of the chain.")
  (policy
   (string "accept")
   "Priority of the chain.")
  (rules
   (list-of-my-nftables-rules '()) ;list of <my-nftables-rule>
   "Rules of this chain."))

(define (serialize-my-nftables-chain chain)
  (string-append
   "  chain " (my-nftables-chain-name chain) " {\n"
   "    type " (my-nftables-chain-type chain)
   " hook " (my-nftables-chain-hook chain)
   " priority " (my-nftables-chain-priority chain)
   "; policy " (my-nftables-chain-policy chain) ";\n"
   (serialize-list-of-my-nftables-rules
    (my-nftables-chain-rules chain))
   "  }\n"))





(define (list-of-my-nftables-chains? lst)
  (every my-nftables-chain? lst))

(define (serialize-list-of-my-nftables-chains chains)
  (string-join
   (map serialize-my-nftables-chain chains)
   "\n"))





(define-configuration
  my-nftables-table
  (name
   (string)
   "Name of this table.")
  (type
   (string)
   "Type of this table.")
  (additional
   (string)
   "Additional content of the table (sets, maps, ...)")
  (chains
   (list-of-my-nftables-chains '())
   "Chains of this table."))

(define (serialize-my-nftables-table table)
  (string-append
   "table "
   (my-nftables-table-type table) " "
   (my-nftables-table-name table) " {\n"
   (my-nftables-table-additional table)
   (serialize-list-of-my-nftables-chains
    (my-nftables-table-chains table))
   "}\n"))





(define (serialize-my-nftables-configuration configuration)
  (string-append
   "flush ruleset\n\n"
   (string-join
    (map serialize-my-nftables-table configuration)
    "\n")))





					; TODO:
					; Remove the following helper once the rules are structured.
(define (make-rules rules)
  (map
   (lambda (r)
     (my-nftables-rule (rule r)))
   rules))



(define (my-nftables->file name configuration)
  (plain-file name
              (serialize-my-nftables-configuration
               configuration)))



(define (my-nftables-make-file tcp-ports udp-ports)
  (my-nftables->file "nftables.conf"
		     (list
		      (my-nftables-table
		       (name "filter")
		       (type "inet")
		       (additional
			"
          set bl4 {
          type ipv4_addr
          }

          set bl6 {
          type ipv6_addr
          }

          set wl4 {
          type ipv4_addr
          flags interval
          elements = { 192.168.51.0/24 }
          }

          ")
		       (chains
			(list
			 (my-nftables-chain
			  (name "input")
			  (type "filter")
			  (hook "input")
			  (priority "filter")
			  (policy "drop")
			  (rules
			   (make-rules
			    (list
			     "meta iiftype loopback accept"
			     "ip saddr @wl4 accept"
			     "ct state invalid drop"
			     "ip saddr @bl4 drop"
			     "ip6 saddr @bl6 drop"
			     "ct state { established, related } accept"
			     (if
                              (null? tcp-ports)
                              ""
                              (string-append
                               "tcp dport { "
                               (string-join
				(map number->string tcp-ports) ", ")
                               " } ct state new accept"))
			     (if
                              (null? udp-ports)
                              ""
                              (string-append
                               "udp dport { "
                               (string-join
				(map number->string udp-ports) ", ")
                               " } ct state new accept"))
			     "meta l4proto { icmp, ipv6-icmp } accept"
			     "add @bl4 { ip daddr }"
			     "add @bl6 { ip6 daddr }"))))
			 (my-nftables-chain
			  (name "forward")
			  (type "filter")
			  (hook "forward")
			  (priority "filter")
			  (policy "drop")
			  (rules
			   (make-rules '())))
			 (my-nftables-chain
			  (name "output")
			  (type "filter")
			  (hook "output")
			  (priority "filter")
			  (policy "accept")
			  (rules
			   (make-rules '())))))))))

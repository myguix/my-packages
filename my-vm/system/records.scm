(define-module (my-vm system records)
  #:use-module (guix records)
  #:export (vm-configuration
	    vm-configuration-id
	    vm-configuration-name
	    vm-configuration-key
	    vm-configuration-services
	    vm-configuration-packages
	    vm-configuration-tcp-ports
	    vm-configuration-udp-ports))

(define-record-type* <vm-configuration> vm-configuration
  make-vm-configuration
  vm-configuration?
  (id vm-configuration-id
      (default #f))
  (name vm-configuration-name
        (default "noname"))
  (key vm-configuration-key
       #f)
  (services vm-configuration-services
            (default '()))
  (packages vm-configuration-packages
            (default '()))
  (tcp-ports vm-configuration-tcp-ports
             (default '()))
  (udp-ports vm-configuration-udp-ports
             (default '())))

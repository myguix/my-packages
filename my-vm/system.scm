(define-module (my-vm system)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services)
  #:use-module (gnu services ssh)
  #:use-module (gnu services networking)
  #:use-module (gnu machine)
  #:use-module (gnu machine ssh)
  #:use-module (gnu packages vim)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages tmux)
  #:use-module (gnu packages certs)
  #:use-module (gnu packages ssh)
  #:use-module (guix gexp)
  #:use-module (guix modules)

  #:use-module (my-vm system records)
  #:use-module (my-vm system firewall)
  #:export (vm-configuration->system
            vm-configuration->os-system))

(define %vm-user "arnaud")
(define %vm-ssh-key
  (plain-file
   "ds-ac.pub"
   "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC+Ee5pQFVf/sTiAw8+yrUaIVXCLqcPKAblL070sCL8y ds-ac@portableArnaud"))
(define %my-authorized-guix-keys
  (cons
    (plain-file
      "signing-key.pub"
      "(public-key
(ecc
    (curve Ed25519)
      (q #2E03A85A4FC6453291C75878A317372BC70F52D6494BDC1292D25DFD6B366880#)
        )
 )")
    %default-authorized-guix-keys))

(define %vm-base-packages
  (cons* vim nftables htop openssh tmux nss-certs
	 %base-packages))
(define %vm-file-systems
  (cons* (file-system
	  (device (file-system-label "my-root"))
	  (mount-point "/")
	  (type "ext4"))
	 (file-system
	  (device (file-system-label "my-boot"))
	  (mount-point "/boot")
	  (type "ext2"))
	 %base-file-systems))
(define %vm-users
  (cons (user-account
	 (name %vm-user)
	 (comment "Main user of the VM")
	 (group "users")
	 (supplementary-groups '("wheel")))
	%base-user-accounts))

(define (vm-base-services vmid tcps udps)
  (cons*
					; Networking.
   (service static-networking-service-type
	    (list
	     (static-networking
	      (addresses
	       (list
		(network-address
		 (device "eth0")
		 (value
		  (string-append "2a0c:700:12:50:1::"
				 (number->string vmid)
				 "/80")))
		(network-address
		 (device "eth0")
		 (value
		  (string-append "192.168.51."
				 (number->string vmid)
				 "/24")))))
	      (routes
	       (list
		(network-route
		 (destination "default")
		 (gateway "2a0c:700:12:50:1::1"))
		(network-route
		 (destination "default")
		 (gateway "192.168.51.1"))))
	      (name-servers '("192.168.51.102")))))

					; Firewall.
   (service nftables-service-type
	    (nftables-configuration
	     (ruleset (my-nftables-make-file tcps udps))))

					; OpenSSH.
   (service openssh-service-type
	    (openssh-configuration
	     (password-authentication? #f)
	     (authorized-keys
	      `((,%vm-user ,%vm-ssh-key)))
	     (openssh openssh-sans-x)
	     (port-number 22)))

					; Below: base Guix services, and the key
					; to allow sending store-elements.
   (modify-services
	    %base-services
	    (guix-service-type config =>
			       (guix-configuration
				(inherit config)
				(authorized-keys
          %my-authorized-guix-keys))))))

(define (vm-configuration->host-name configuration)
  (string-append
   (number->string
    (vm-configuration-id configuration))
   "-"
   (vm-configuration-name configuration)))

(define (vm-configuration->os-system configuration)
  (let
      ((vm-id (vm-configuration-id configuration))
       (vm-name (vm-configuration-name configuration))
       (vm-services (vm-configuration-services configuration))
       (vm-packages (vm-configuration-packages configuration))
       (vm-udp-ports (vm-configuration-udp-ports configuration))
       (vm-tcp-ports (vm-configuration-tcp-ports configuration)))

    (operating-system
     (host-name (vm-configuration->host-name configuration))

     (timezone "Europe/Paris")
     (locale "en_US.utf8")
     (keyboard-layout (keyboard-layout "fr"))

     (initrd-modules
      (cons "virtio_scsi" %base-initrd-modules))
     (kernel-arguments (list "console=ttyS0,115200"))
     (bootloader (bootloader-configuration
		  (bootloader grub-bootloader)
		  (targets '("/dev/sda"))))

     (file-systems %vm-file-systems)
     (users %vm-users)

     (sudoers-file
      (plain-file "sudoers"
		  (string-append (plain-file-content %sudoers-specification)
				 (format #f "~a ALL = NOPASSWD: ALL~%"
					 %vm-user))))

     (packages (append vm-packages %vm-base-packages))

     (services
      (append vm-services
	      (vm-base-services vm-id vm-tcp-ports vm-udp-ports))))))

(define (vm-configuration->system configuration)
  (machine
   (operating-system (vm-configuration->os-system configuration))
   (environment managed-host-environment-type)
   (configuration
    (machine-ssh-configuration
     (authorize? #t)
     (host-name (vm-configuration->host-name configuration))
     (system "x86_64-linux")
     (user %vm-user)
     (identity "/home/arnaud/.ssh/id_archimel")
     (host-key (vm-configuration-key configuration))))))

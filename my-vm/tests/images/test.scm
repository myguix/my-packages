(define-module (my-vm tests images test)
  #:use-module (guix gexp)
  #:use-module (gnu)
  #:use-module (gnu image)
  #:use-module (gnu system image)
  #:use-module (gnu services)
  #:use-module (gnu services dns)
  #:use-module (my-vm system records)
  #:use-module (my-vm system)
  #:export (kresd-vm))

(define (kresd-vm vmid ssh-key)
  (vm-configuration
   (id vmid)
   (name "kresd")
   (key ssh-key)
   (services
    (list
        (service
          knot-resolver-service-type
          (knot-resolver-configuration
            (kresd-config-file
              (plain-file "kresd.conf"
"-- -*- mode: lua -*-
net = { '0.0.0.0', '::1' }
user('knot-resolver', 'knot-resolver')
modules = { 'hints > iterate', 'stats', 'predict' }
cache.size = 100 * MB
"))))))
   (udp-ports '(53))
   (tcp-ports '(22 953))))


(image
  (name "test-kresd")
  (format 'iso9660)
  (operating-system
    (vm-configuration->os-system
      (kresd-vm
        666
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA6cCqhVPJi+GKpJ0QsbvM8n5nRdXWHNPrLPlhGK+zXQ"))))

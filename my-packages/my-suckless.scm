(define-module (my-packages my-suckless)
  #:use-module (guix download)
  #:use-module (gnu packages suckless)
  #:use-module (guix packages))

(define-public my-dwm
  (package
    (inherit dwm)
    (name "my-dwm")
    (version "6.4-1")
    (source (origin
             (method url-fetch)
             (uri (string-append "https://nanein.fr/packages/dwm-"
                                 "6.4-0" ".tar.gz"))
             (sha256
              (base32
                "1rz602fihf547nxs53zm9zbaspr01xj74y4m8y1fp6nsfyqvwvi6"))))))

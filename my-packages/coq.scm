(define-module (my-packages coq)
  #:use-module (gnu packages coq)
  #:use-module (gnu packages)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages emacs)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages ocaml)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages rsync)
  #:use-module (gnu packages texinfo)
  #:use-module (guix build-system dune)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((srfi srfi-1) #:hide (zip)))

(define-public coq-iris
  (package
    (name "coq-iris")
    (version "3.6.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.mpi-sws.org/iris/iris.git")
                     (commit (string-append "iris-" version))))
              (file-name (git-file-name name version))
              (sha256
                (base32
                  "02vbq597fjxd5znzxdb54wfp36412wz2d4yash4q8yddgl1kakmj"))))
    (build-system gnu-build-system)
    (inputs (list coq coq-stdpp))
    (arguments
      `(#:tests? #f ; Tests are executed during build phase.
        #:make-flags (list (string-append "COQLIBINSTALL="
                                          (assoc-ref %outputs "out")
                                          "/lib/coq/user-contrib"))
        #:phases
        (modify-phases %standard-phases
                       (delete 'configure))))
    (synopsis
      "A Higher-Order Concurrent Separation Logic Framework with support for 
      interactive proofs")
    (description
      "Iris is a framework for reasoning about the safety of concurrent 
      programs using concurrent separation logic. It can be used to develop 
      a program logic, for defining logical relations, and for reasoning about 
      type systems, among other applications. This package includes the base 
      logic, Iris Proof Mode (IPM) / MoSeL, and a general language-independent 
      program logic; see coq-iris-heap-lang for an instantiation of the 
      program logic to a particular programming language.")
    (home-page "https://iris-project.org/")
    (license bsd-3)))

(define-public coq-stdpp-dev
  (package
    (inherit coq-stdpp)
    (name "coq-stdpp")
    (version "23062800-dev")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.mpi-sws.org/iris/stdpp.git")
                    (commit "592f728fd143c7ab3949d06c987066ed8000234f")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                 "1q7bx2qlvc3y1z6jqf35khnr32i4mv2jhvisk6wchh8lxd1qdiq8"))))))

(define-public coq-iris-dev
  (package
    (inherit coq-iris)
    (name "coq-iris-dev")
    (version "23071700-dev")
    (inputs (list coq coq-stdpp-dev))
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://gitlab.mpi-sws.org/iris/iris.git")
                     (commit "7e865892f631202263ae4b888d7f74437f9cb56e")))
              (file-name (git-file-name name version))
              (sha256
                (base32
                  "0zi9png0dj0pglwgk0aiwhv02ripn7222ch347cyyxr6a1i252x5"))))))

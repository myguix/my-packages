(define-module (my-packages web)
               #:use-module (guix build-system trivial)
               #:use-module (guix packages)
               #:use-module (guix download)
               #:use-module (guix git-download)
               #:use-module (guix gexp)
               #:use-module (gnu packages base)
               #:use-module (gnu packages emacs)
               #:use-module (gnu packages emacs-xyz)
               #:use-module (guix licenses))

(define-public
  static-nanein-website
  (let ((commit "4946805c544f2bd25df524ec9aa195337addde74")
        (revision "1")
        (%version "0.1.0")
        (hash "1cyw18n38j4y2jby3c0nc39xh9r0xlllrx8lkdbrmfwjqk1hmr38"))

    (package
      (name "static-nanein-website")
      (version (git-version %version revision commit))

      (source (origin
                (method git-fetch)
                (uri (git-reference
                       (url "https://gitlab.crans.org/myguix/static-nanein")
                       (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                  (base32 hash))))

      (inputs (list emacs-org emacs coreutils emacs-htmlize))
      (build-system trivial-build-system)

      (arguments
        (list
          #:modules `((guix build utils))

          #:builder
          #~(begin
              (use-modules (guix build utils))
              (use-modules (ice-9 ftw)
                           (ice-9 string-fun)
                           (ice-9 match))

              (define (translate-one-file filename)
                (system
                  (string-append
                    (assoc-ref %build-inputs "emacs") "/bin/emacs "
                    filename
                    " --batch -f org-html-export-to-html --kill")))

              (define (walk src-path out-path)
                (match (stat:type (stat src-path))
                       ('directory
                        (begin
                          ;; (format #t "Directory: ~a\n" src-path)
                          (for-each
                            (lambda (child)
                              ; If the child is a file, the output should remain 
                              ; unchanged for the copy to succeed later.
                              (let* ((next-src
                                       (string-append src-path "/" child))
                                     (next-out
                                       (if (eq? (stat:type (stat next-src))
                                                'regular)
                                         out-path
                                         (string-append out-path "/" child))))
                                (walk next-src next-out)))
                            (scandir src-path
                                     (lambda (s)
                                       (and
                                         (not (string=? s "."))
                                         (not (string=? s ".."))))))))

                       ('regular
                        (if (eq? (string-suffix-length src-path ".org") 4)
                          ;; The file is ideed an Org file
                          (let ((src-res-path ; file.org => file.html
                                  (string-append (string-drop-right src-path 3)
                                                 "html")))
                            (begin
                              (format #t "Translating ~s~%" src-path)
                              ;; Translate the file.
                              (translate-one-file src-path)
                              ;; Copy the result...
                              (install-file src-res-path out-path)))

                          ;; The file is *not* an Org file.
                          (install-file src-path out-path)))

                       (_ #t)))


              ;; First recursively copy org files from ~#+source~ to "src", and
              ;; enter the directory.
              (copy-recursively #+source "src")
              (chdir "src")

              (walk (getcwd) %output))))

      (synopsis "My static website for Nanein.")
      (description "This package defines a static website.")
      (home-page "https://nanein.fr/")
      (license gpl3+))))
static-nanein-website
